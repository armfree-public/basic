package main

import (
	"log"
	"os"

	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"

	"basic/pkg/handler"
	"basic/pkg/router"
	"basic/pkg/service"
	"basic/pkg/storage"
)

func init() {
	log.SetFlags(log.LstdFlags | log.Llongfile)
}

func main() {
	if err := godotenv.Load(".env"); err != nil {
		log.Fatal(err)
	}

	repo := storage.NewPostgresStorage(
		os.Getenv("HOST"),
		os.Getenv("PORT"),
		os.Getenv("USER"),
		os.Getenv("PASS"),
		os.Getenv("NAME"),
	)

	srv := service.NewService(repo)

	hndl := handler.NewService(srv)

	engine := gin.New()
	engine.Use(gin.Logger())
	engine.Use(gin.Recovery())
	base := engine.Group(os.Getenv("BASE_PATH"))
	router.RegisterRoutes(base, hndl)
	log.Fatal(engine.Run(os.Getenv("ADDRESS")))
}
