package router

import (
	"github.com/gin-gonic/gin"

	"basic/pkg/handler"
)

func RegisterRoutes(engine *gin.RouterGroup, hndl handler.Handler) {
	engine.GET("/", hndl.GetAll)
}
