package handler

import (
	"github.com/gin-gonic/gin"

	"basic/pkg/service"
)

type Handler interface {
	GetAll(c *gin.Context)
	//TODO: add methods and implement
}

type handler struct {
	srv service.Service
}

func NewService(srv service.Service) Handler {
	return &handler{srv: srv}
}

func (h *handler) GetAll(c *gin.Context) {

}
