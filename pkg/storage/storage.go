package storage

import (
	"fmt"
	"log"

	_ "github.com/jackc/pgx/stdlib"
	"github.com/jmoiron/sqlx"
)

type Storage interface {
	GetAll(information interface{}) (returnValue interface{})
	// TODO: add methods to storage and implement them
}

type storage struct {
	db *sqlx.DB
}

func NewPostgresStorage(host, port, username, password, dbName string) Storage {
	conn, err := sqlx.Connect("pgx", fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable",
		host,
		port,
		username,
		password,
		dbName))
	if err != nil {
		log.Fatalf("Error connecting to database: %s", err.Error())
	}
	return &storage{
		db: conn,
	}
}

func (r *storage) GetAll(information interface{}) (returnValue interface{}) {
	//TODO: սա ընդհամենը օրինակ է, մի օգտագործիր ինտերֆեյսներ
	return nil
}
