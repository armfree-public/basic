package service

import (
	"basic/pkg/storage"
)

type Service interface {
	GetAll(information interface{}) (returnValue interface{})
	//TODO: add methods and implement like this
}

type service struct {
	db storage.Storage
}

func NewService(store storage.Storage) Service {
	return &service{db: store}
}

func (s *service) GetAll(information interface{}) (returnValue interface{}) {
	//TODO: սա ընդհամենը օրինակ է, մի օգտագործիր ինտերֆեյսներ
	return nil
}
